#pragma once

#include "../include/image_read_write.h"

typedef struct __attribute__((packed)) {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bmp_header;

read_status read_bmp(FILE* in, image* img);

write_status write_bmp(FILE* out, const image* img);

size_t get_bmp_padding(const image* img);

size_t get_bmp_pixels_size(const image *img);

size_t get_bmp_size(const image* img);

read_status read_bmp_header(FILE* in, image* img);

read_status read_bmp_pixels(FILE* in, image* img);

write_status write_bmp_header(FILE* out, const image* img);

write_status write_bmp_pixels(FILE* out, const image* img);
