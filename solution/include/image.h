
#pragma once

#include <stdint.h>
#include <stdlib.h>

typedef struct  __attribute__((packed)) {
    uint8_t b, g, r;
} pixel;

typedef struct {
    uint64_t width, height;
    pixel* data;
} image;
