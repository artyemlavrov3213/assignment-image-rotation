#pragma once

#include "../include/image.h"

#include <stdio.h>

typedef enum {
    WRITE_OK = 0,
    WRITE_INVALID_FILE,
    WRITE_UNKNOWN_FORMAT,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_DATA
} write_status;

typedef enum {
    READ_OK = 0,
    READ_INVALID_FILE,
    READ_UNKNOWN_FORMAT,
    READ_INVALID_HEADER,
    READ_INVALID_BITS,
    READ_INVALID_SIGNATURE
} read_status;

typedef struct {
    const char *extension;
    read_status (*read_image)(FILE* in, image* img);
    write_status (*write_image)(FILE* out, const image* img);
} image_format;

image_format* get_file_image_format(const char *path);

read_status read_image(const char* path, image* img);

write_status write_image(const char* path, const image* img);

void release_image(image *img);
