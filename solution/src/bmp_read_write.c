#include "../include/bmp_read_write.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#define BMP_TYPE_MAGIC 0x4D42
#define BMP_RESERVED_MAGIC 0
#define BMP_COMPRESSION_MAGIC 0
#define BMP_PIXEL_PER_METER_MAGIC 2834
#define BMP_PLANES_MAGIC 1
#define BMP_COLORS_MAGIC 0
#define BMP_HEADER_SIZE_MAGIC 40
#define DWORD_SIZE 4
#define COLOR_DEPTH 24

read_status read_bmp(FILE* in, image* img) {
    read_status status = read_bmp_header(in, img);
	if (status != READ_OK) return status;
	return read_bmp_pixels(in, img);
}

write_status write_bmp(FILE* out, const image* img) {
    write_status status = write_bmp_header(out, img);
	if (status != WRITE_OK) return status;
    return write_bmp_pixels(out, img);
}

size_t get_bmp_padding(const image* img) {
	return 4 - (img->width * sizeof(pixel)) % 4;
}

size_t get_bmp_pixels_size(const image *img) {
	return img->height * (img->width * sizeof(pixel) + get_bmp_padding(img));
}

size_t get_bmp_size(const image* img) {
	return sizeof(bmp_header) + get_bmp_pixels_size(img);
}

read_status read_bmp_header(FILE* in, image* img) {
	bmp_header header;
	if (!fread(&header, sizeof(bmp_header), 1, in)) return READ_INVALID_HEADER;
    if (header.bfType != BMP_TYPE_MAGIC) return READ_INVALID_SIGNATURE;
    img->width = header.biWidth;
    img->height = header.biHeight;
	return READ_OK;
}

read_status read_bmp_pixels(FILE* in, image* img) {
	img->data = (pixel*) malloc(get_bmp_pixels_size(img));
	for (size_t row = 0; row < img->height; row++) {
		if (fread(&img->data[row * img->width], sizeof(pixel), img->width, in) != img->width) return READ_INVALID_BITS;
		fseek(in, get_bmp_padding(img), SEEK_CUR);
	}
	return READ_OK;
}

write_status write_bmp_header(FILE* out, const image* img) {
	bmp_header header;
	header.bfType = BMP_TYPE_MAGIC;
	header.biBitCount = COLOR_DEPTH;
	header.biXPelsPerMeter = BMP_PIXEL_PER_METER_MAGIC;
	header.biYPelsPerMeter = BMP_PIXEL_PER_METER_MAGIC;
    header.bfileSize = get_bmp_size(img) + BMP_RESERVED_MAGIC;
    header.bfReserved = BMP_RESERVED_MAGIC;
    header.bOffBits = sizeof(bmp_header);
    header.biSize = BMP_HEADER_SIZE_MAGIC;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BMP_COMPRESSION_MAGIC;
    header.biCompression = BMP_COMPRESSION_MAGIC;
    header.biSizeImage = get_bmp_pixels_size(img);
    header.biClrUsed = BMP_COLORS_MAGIC;
    header.biClrImportant = BMP_COLORS_MAGIC;
    fwrite(&header, sizeof(bmp_header), 1, out);
	return WRITE_OK;
}

write_status write_bmp_pixels(FILE* out, const image* img) {
	for (size_t row = 0; row < img->height; row++) {
		fwrite(&img->data[row * img->width], sizeof(pixel), img->width, out);
		fwrite(img->data, 1, get_bmp_padding(img), out);
	}
	return WRITE_OK;
}