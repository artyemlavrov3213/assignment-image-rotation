#include "../include/image_transform.h"
#include <stdio.h>

image *rotate(const image* src) {
    image *dst = (image*) malloc(sizeof(image));
    dst->width = src->height;
    dst->height = src->width;
    dst->data = (pixel*) malloc(sizeof(pixel) * dst->width * dst->height);
    for (size_t i = 0; i < dst->height; i++) {
        for (size_t j = 0; j < dst->width; j++) {
            dst->data[i * dst->width + j] = src->data[(src->height - j - 1) * src->width + i];
        }
    }
    return dst;
}
