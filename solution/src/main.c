#include <stdio.h>
#include <stdlib.h>

#include "../include/image_read_write.h"
#include "../include/image_transform.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        printf("Correct format : image-transform `dst` `src`");
        return -1;
    }
    image *src = malloc (sizeof(image));
    {
        read_status status = read_image(argv[1], src);
        if (status != READ_OK) {
            release_image(src);
            printf("READ ERROR: %d", status);
            return status;
        }
    }
    image *dst = rotate(src);
    release_image(src);
    {    
        write_status status = write_image(argv[2], dst);
        release_image(dst);
        if (status != WRITE_OK) {
            printf("WRITE ERROR: %d", status);
            return status;
        }
    }
    return 0;
}
