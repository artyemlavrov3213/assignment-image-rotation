#include "../include/bmp_read_write.h"
#include "../include/image_read_write.h"
#include <ctype.h>
#include <string.h>

#define FORMATS_COUNT 1
image_format image_formats[FORMATS_COUNT] = {
    { "bmp", &read_bmp, &write_bmp }
};

image_format* get_file_image_format(const char *path) {
    for (char *p = (char*) path; *p; p++) *p = (char) tolower((int)(*p));
    const char *dot = strrchr(path, '.');
    if(!dot || dot == path) return NULL;
    for (size_t i = 0; i < FORMATS_COUNT; i++) {
        if (strcmp(dot + 1, image_formats[i].extension) == 0) {
            return &image_formats[i];
        }
    }
    return NULL;
}

read_status read_image(const char *path, image* img) {
    image_format* format = get_file_image_format(path);
    if (!format) return READ_UNKNOWN_FORMAT;
    FILE* file = fopen(path, "r");
    if (!file) return READ_INVALID_FILE;
    read_status status = format->read_image(file, img);
    fclose(file);
    return status;
}

void release_image(image *img) {
    free(img->data);
    free(img);
}

write_status write_image(const char* path, image const* img) {
    image_format* format = get_file_image_format(path);
    if (!format) return WRITE_UNKNOWN_FORMAT;
    FILE* file = fopen(path, "w");
    if (!file) return WRITE_INVALID_FILE;
    write_status status = format->write_image(file, img);
    fclose(file);
    return status;
}
